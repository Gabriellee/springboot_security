/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost
 Source Database       : blog

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : utf-8

 Date: 01/24/2019 19:34:07 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `person`
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `key` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `motherId` int(11) DEFAULT NULL,
  `fatherId` int(11) DEFAULT NULL,
  `gender` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dateOfBirth` int(11) DEFAULT NULL,
  PRIMARY KEY (`key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `person`
-- ----------------------------
BEGIN;
INSERT INTO `person` VALUES ('1', 'King George VI', '0', '0', '', '0'), ('2', 'Queen Elizabeth', '0', '0', null, '0'), ('3', 'Queen Elizabeth II', '2', '1', null, '0'), ('4', 'Princess Margaret', '2', '1', null, '0'), ('5', 'Prince Philip Duke of Edinburgh', '0', '0', null, '0'), ('6', 'Prince Charles', '3', '5', null, '0'), ('7', 'Princess Diana', '0', '0', null, '0'), ('8', 'Prince William', '7', '6', null, '0'), ('9', 'Prince Harry', '7', '6', null, '0'), ('10', 'Catherine Duchess20%of20Êmbridge', '0', '0', null, '0'), ('11', 'Prince George', '10', '8', null, '0'), ('12', 'Princess Charlotte', '10', '8', 'female', '20150502'), ('13', 'Prince Louis', '10', '8', 'male', '14830626'), ('123', '', '2', '1', 'null', '0');
COMMIT;

-- ----------------------------
--  Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `role`
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES ('1', 'ROLE_ADMIN'), ('2', 'SCOPE_READ'), ('3', 'ROLE_USER');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_fe6nqh4mlcjr068gcfrstmh2y` (`role_id`) USING BTREE,
  CONSTRAINT `FK_fe6nqh4mlcjr068gcfrstmh2y` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('aa', 'aa', '1', '1', 'admin'), ('bb', 'bb', '2', '2', 'admin'), ('cc', 'cc', '3', '3', 'admin,user');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
