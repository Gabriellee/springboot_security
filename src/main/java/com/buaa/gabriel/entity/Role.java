package com.buaa.gabriel.entity;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan
public class Role {
    private int id;
    private String role;
}
