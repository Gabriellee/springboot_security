package com.buaa.gabriel.dao;

import com.buaa.gabriel.entity.User;
import com.buaa.gabriel.entity.User_con;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    public int insert(User record);
    @Select("select * from user where user.username=#{username,jdbcType=VARCHAR}")
    public User_con select(String username);
}
