package com.buaa.gabriel;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
@MapperScan("com.buaa.gabriel.dao")
public class Example {
    public static void main(String[] args) {
        SpringApplication.run(Example.class, args);
    }
}