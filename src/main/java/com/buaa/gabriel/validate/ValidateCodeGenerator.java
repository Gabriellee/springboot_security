package com.buaa.gabriel.validate;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * 验证码生成器
 * Created by Fant.J.
 */
public interface ValidateCodeGenerator {
    /**
     * 创建验证码
     */
    ImageCode  createCode(ServletWebRequest request);
}