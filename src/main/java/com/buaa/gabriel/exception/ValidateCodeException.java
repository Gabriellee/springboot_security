package com.buaa.gabriel.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义 验证码异常类
 * Created by Fant.J.
 */
public class ValidateCodeException extends AuthenticationException {
    public ValidateCodeException(String msg) {
        super(msg);
    }
}