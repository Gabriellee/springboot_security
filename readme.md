# springboot入门项目
## 登录功能
> 请求路径为.html结尾时，若未登录则跳转至home页面，home页面必须有admin权限才可以登录
> 请求路径不以.html结尾时，则返回json信息

## 验证码功能
> 在security的验证用户名和密码的过滤器前增加验证码过滤器

## ORM框架
> 使用mybatis